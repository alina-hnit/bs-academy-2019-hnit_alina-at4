const fetch = require('node-fetch');
{
    const url = 'http://165.227.137.250/api/v1'
    const args = require('./testData.json');

    class List {
        static async sendCreateListRequest(url, args) {
            return fetch(`${url}/auth/login`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    name: args.newListName
                })
            });
        };
    };
    
    async function createList(url, args) {

        console.log(`Creating list for ${url}`);
        const response = await Client.sendCreateListRequest(url, args);

        if (response.status === 200) {
            console.log(`List is created on ${url}`);
            return Promise.resolve();
        }
        const responseJSON = await response.json();
        const error = new Error(`Failed to successfully create list for ${url}`);
        error.message = '' + JSON.stringify(responseJSON.error);
        return Promise.reject(error);
        
    }
    
    function handleError(error) {
        const errorBody = () => {
            return error && error.message ? error.message : error;
        };
        console.log('Error during bootstrap_Create_List, exiting', errorBody());
        process.exit(1);

    };

    module.exports = (async done => {
        console.log('========Start=========');
        console.log('========Creating User=========');
        createList(url, args)
            .then(() => {})
            .catch(error => {
                done(handleError(error));
            })
    });
    
}
